import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Credential } from '../auth.model';

@Component({
  selector: 'm79-auth-form',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  @Input()
  hasAuthError: boolean | null = false;

  @Output() 
  submit = new EventEmitter<Credential>();

  usernameControl = new FormControl('', [Validators.required]);
  passwordControl = new FormControl('', [Validators.required]);

  form = new FormGroup({
    username: this.usernameControl,
    password: this.passwordControl
  })

  constructor() { }

  ngOnInit(): void {
  }

  triggerSubmit(isFormValid: boolean) {
    if(isFormValid) {
      const credentials = this.form.value;

      this.submit.next({
        username: credentials.username, 
        password: credentials.password 
      });
    }
  }
}
