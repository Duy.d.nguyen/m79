import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isLoggedIn = false;
  private MOCK_USERNAME = 'admin';
  private MOCK_PASSWORD = 'password';

  constructor() { 
  }

  performAuth(username: string, password: string) {
    // Performing fake authentication
    this.isLoggedIn = username === this.MOCK_USERNAME && password === this.MOCK_PASSWORD;

    return this.isLoggedIn;
  }

  isUserLoggedIn() {
    return this.isLoggedIn;
  }
}
