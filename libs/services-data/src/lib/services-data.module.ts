import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromServices from './+state/services/services.reducer';
import { ServicesEffects } from './+state/services/services.effects';
import { ServicesFacade } from './+state/services/services.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromServices.SERVICES_FEATURE_KEY,
      fromServices.reducer
    ),
    EffectsModule.forFeature([ServicesEffects]),
  ],
  providers: [ServicesFacade],
})
export class ServicesDataModule {}
