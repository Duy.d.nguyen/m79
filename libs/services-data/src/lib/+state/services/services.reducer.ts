import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';

import * as ServicesActions from './services.actions';
import { ServicesEntity } from './services.models';

export const SERVICES_FEATURE_KEY = 'services';

export interface State extends EntityState<ServicesEntity> {
  selectedId?: string | number; // which Services record has been selected
  loaded: boolean; // has the Services list been loaded
  error?: string | null; // last known error (if any)
}

export interface ServicesPartialState {
  readonly [SERVICES_FEATURE_KEY]: State;
}

export const servicesAdapter: EntityAdapter<ServicesEntity> =
  createEntityAdapter<ServicesEntity>();

export const initialState: State = servicesAdapter.getInitialState({
  // set initial required properties
  loaded: false,
});

const servicesReducer = createReducer(
  initialState,
  on(ServicesActions.init, (state) => ({
    ...state,
    loaded: false,
    error: null,
  })),
  on(
    ServicesActions.loadServicesSuccess, 
    ServicesActions.setMockData,
    (state, { services }) =>
    servicesAdapter.setAll(services, { ...state, loaded: true })
  ),
  on(ServicesActions.loadServicesFailure, (state, { error }) => ({
    ...state,
    error,
  })),
  on(ServicesActions.updateService, (state, { service }) => servicesAdapter.updateOne({
    id: service.id,
    changes: {
      name: service.name,
      startDate: service.startDate
    }
  }, { ...state })),
  on(ServicesActions.addService, (state, {service}) => servicesAdapter.addOne(service, {...state}))
);

export function reducer(state: State | undefined, action: Action) {
  return servicesReducer(state, action);
}
