import { ServicesEntity } from './services.models';
import {
  servicesAdapter,
  ServicesPartialState,
  initialState,
} from './services.reducer';
import * as ServicesSelectors from './services.selectors';

describe('Services Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getServicesId = (it: ServicesEntity) => it.id;
  const createServicesEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
    } as ServicesEntity);

  let state: ServicesPartialState;

  beforeEach(() => {
    state = {
      services: servicesAdapter.setAll(
        [
          createServicesEntity('PRODUCT-AAA'),
          createServicesEntity('PRODUCT-BBB'),
          createServicesEntity('PRODUCT-CCC'),
        ],
        {
          ...initialState,
          selectedId: 'PRODUCT-BBB',
          error: ERROR_MSG,
          loaded: true,
        }
      ),
    };
  });

  describe('Services Selectors', () => {
    it('getAllServices() should return the list of Services', () => {
      const results = ServicesSelectors.getAllServices(state);
      const selId = getServicesId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelected() should return the selected Entity', () => {
      const result = ServicesSelectors.getSelected(state) as ServicesEntity;
      const selId = getServicesId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getServicesLoaded() should return the current "loaded" status', () => {
      const result = ServicesSelectors.getServicesLoaded(state);

      expect(result).toBe(true);
    });

    it('getServicesError() should return the current "error" state', () => {
      const result = ServicesSelectors.getServicesError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
