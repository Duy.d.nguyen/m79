import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  SERVICES_FEATURE_KEY,
  State,
  servicesAdapter,
} from './services.reducer';

// Lookup the 'Services' feature state managed by NgRx
export const getServicesState =
  createFeatureSelector<State>(SERVICES_FEATURE_KEY);

const { selectAll, selectEntities } = servicesAdapter.getSelectors();

export const getServicesLoaded = createSelector(
  getServicesState,
  (state: State) => state.loaded
);

export const getServicesError = createSelector(
  getServicesState,
  (state: State) => state.error
);

export const getAllServices = createSelector(getServicesState, (state: State) =>
  selectAll(state)
);

export const getServicesEntities = createSelector(
  getServicesState,
  (state: State) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getServicesState,
  (state: State) => state.selectedId
);

export const getSelected = createSelector(
  getServicesEntities,
  getSelectedId,
  (entities, selectedId) => (selectedId ? entities[selectedId] : undefined)
);
