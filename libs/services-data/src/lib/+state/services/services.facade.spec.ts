import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';
import { NxModule } from '@nrwl/angular';
import { readFirst } from '@nrwl/angular/testing';

import * as ServicesActions from './services.actions';
import { ServicesEffects } from './services.effects';
import { ServicesFacade } from './services.facade';
import { ServicesEntity } from './services.models';
import {
  SERVICES_FEATURE_KEY,
  State,
  initialState,
  reducer,
} from './services.reducer';
import * as ServicesSelectors from './services.selectors';

interface TestSchema {
  services: State;
}

describe('ServicesFacade', () => {
  let facade: ServicesFacade;
  let store: Store<TestSchema>;
  const createServicesEntity = (id: string, name = ''): ServicesEntity => ({
    id,
    name: name || `name-${id}`,
  });

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature(SERVICES_FEATURE_KEY, reducer),
          EffectsModule.forFeature([ServicesEffects]),
        ],
        providers: [ServicesFacade],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule,
        ],
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.inject(Store);
      facade = TestBed.inject(ServicesFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async () => {
      let list = await readFirst(facade.allServices$);
      let isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(0);
      expect(isLoaded).toBe(false);

      facade.init();

      list = await readFirst(facade.allServices$);
      isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(0);
      expect(isLoaded).toBe(true);
    });

    /**
     * Use `loadServicesSuccess` to manually update list
     */
    it('allServices$ should return the loaded list; and loaded flag == true', async () => {
      let list = await readFirst(facade.allServices$);
      let isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(0);
      expect(isLoaded).toBe(false);

      store.dispatch(
        ServicesActions.loadServicesSuccess({
          services: [createServicesEntity('AAA'), createServicesEntity('BBB')],
        })
      );

      list = await readFirst(facade.allServices$);
      isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(2);
      expect(isLoaded).toBe(true);
    });
  });
});
