/**
 * Interface for the 'Services' data
 */
export interface ServicesEntity {
  id: number; // Primary ID
  name: string;
  startDate: string;
}
