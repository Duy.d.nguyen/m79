import { Injectable } from '@angular/core';
import { select, Store, Action } from '@ngrx/store';
import { map } from 'rxjs/operators';

import * as ServicesActions from './services.actions';
import { ServicesEntity } from './services.models';
import * as ServicesFeature from './services.reducer';
import * as ServicesSelectors from './services.selectors';

@Injectable()
export class ServicesFacade {
  private MOCK_DATA: ServicesEntity[] = [
    {
      id: 1,
      name: 'Service 1',
      startDate: new Date().toUTCString()
    },
    {
      id: 2,
      name: 'Service 2',
      startDate: new Date().toUTCString()
    },
    {
      id: 3,
      name: 'Service 3',
      startDate: new Date().toUTCString()
    },
    {
      id: 4,
      name: 'Service 4',
      startDate: new Date().toUTCString()
    },
    {
      id: 5,
      name: 'Service 5',
      startDate: new Date().toUTCString()
    },
    {
      id: 6,
      name: 'Service 6',
      startDate: new Date().toUTCString()
    },
    {
      id: 7,
      name: 'Service 7',
      startDate: new Date().toUTCString()
    },
    {
      id: 8,
      name: 'Service 8',
      startDate: new Date().toUTCString()
    },
    {
      id: 9,
      name: 'Service 9',
      startDate: new Date().toUTCString()
    },
    {
      id: 10,
      name: 'Service 10',
      startDate: new Date().toUTCString()
    },
  ]
  
  /**
   * Combine pieces of state using createSelector,
   * and expose them as observables through the facade.
   */
  loaded$ = this.store.pipe(select(ServicesSelectors.getServicesLoaded));
  allServices$ = this.store.pipe(select(ServicesSelectors.getAllServices));
  selectedServices$ = this.store.pipe(select(ServicesSelectors.getSelected));

  constructor(private readonly store: Store) {
    this.setMockData();
  }

  /**
   * Use the initialization action to perform one
   * or more tasks in your Effects.
   */
  init() {
    this.store.dispatch(ServicesActions.init());
  }

  setMockData() {
    this.store.dispatch(ServicesActions.setMockData({services: this.MOCK_DATA}));
  }

  getServiceById(id: number) {
    return this.allServices$.pipe(
      map((services) => services.find((service) => Number(service.id) === Number(id)) || undefined)
    )
  }

  updateService(service: ServicesEntity) {
    this.store.dispatch(ServicesActions.updateService({service}));
  }

  addService(service: ServicesEntity) {
    this.store.dispatch(ServicesActions.addService({service}));
  }
}
