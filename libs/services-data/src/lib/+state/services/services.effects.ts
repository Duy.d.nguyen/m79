import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';

import * as ServicesActions from './services.actions';
import * as ServicesFeature from './services.reducer';

@Injectable()
export class ServicesEffects {
  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ServicesActions.init),
      fetch({
        run: (action) => {
          return ServicesActions.loadServicesSuccess({ services: [] });
        },
        onError: (action, error) => {
          console.error('Error', error);
          return ServicesActions.loadServicesFailure({ error });
        },
      })
    )
  );

  constructor(private readonly actions$: Actions) {}
}
