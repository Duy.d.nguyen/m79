import { Action } from '@ngrx/store';

import * as ServicesActions from './services.actions';
import { ServicesEntity } from './services.models';
import { State, initialState, reducer } from './services.reducer';

describe('Services Reducer', () => {
  const createServicesEntity = (id: string, name = ''): ServicesEntity => ({
    id,
    name: name || `name-${id}`,
  });

  describe('valid Services actions', () => {
    it('loadServicesSuccess should return the list of known Services', () => {
      const services = [
        createServicesEntity('PRODUCT-AAA'),
        createServicesEntity('PRODUCT-zzz'),
      ];
      const action = ServicesActions.loadServicesSuccess({ services });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as Action;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
