import { createAction, props } from '@ngrx/store';
import { ServicesEntity } from './services.models';

export const init = createAction('[Services Page] Init');

export const loadServicesSuccess = createAction(
  '[Services/API] Load Services Success',
  props<{ services: ServicesEntity[] }>()
);

export const loadServicesFailure = createAction(
  '[Services/API] Load Services Failure',
  props<{ error: any }>()
);

export const setMockData = createAction(
  '[Services/API] Mock data',
  props<{ services: ServicesEntity[] }>()
)

export const updateService = createAction(
  '[Services/API] Update Service',
  props<{ service: ServicesEntity }>()
)

export const addService = createAction(
  '[Services/API] Add Service',
  props<{ service: ServicesEntity }>()
)
