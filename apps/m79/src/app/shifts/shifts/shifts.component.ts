import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'm79-shifts',
  templateUrl: './shifts.component.html',
  styleUrls: ['./shifts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShiftsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
