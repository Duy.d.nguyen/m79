import { ServicesEntity } from "@m79/services-data";

export interface FormChangedEvent extends Pick<ServicesEntity, 'name' | 'startDate'>{
    valid: boolean
}