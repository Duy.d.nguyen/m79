import { ChangeDetectionStrategy, Component, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ServicesEntity } from '@m79/services-data';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormChangedEvent } from './service-form.model';

@Component({
  selector: 'm79-service-form',
  templateUrl: './service-form.component.html',
  styleUrls: ['./service-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServiceFormComponent implements OnInit {

  @Input()
  set service(s: ServicesEntity) {
    this.originalDateString = s.startDate;
    this.nameControl.setValue(s.name);
    this.dateControl.setValue(new Date(s.startDate))
  }

  @Input()
  viewOnly: boolean = false;

  originalDateString = ''

  nameControl = new FormControl('', [Validators.required]);
  dateControl = new FormControl('', [Validators.required]);

  form = new FormGroup({
    name: this.nameControl,
    date: this.dateControl
  })

  @Output() 
  formChanged: Observable<FormChangedEvent> = this.form.valueChanges.pipe(
    map(() => ({
      valid: this.form.valid,
      name: this.nameControl.value, 
      startDate: this.dateControl.value
    }))
  )

  constructor() { }

  ngOnInit(): void {
  }

}
