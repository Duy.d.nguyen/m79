import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesFacade } from '@m79/services-data';
import { BehaviorSubject, map } from 'rxjs';
import { FormChangedEvent } from '../service-form/service-form.model';

@Component({
  selector: 'm79-service-new',
  templateUrl: './service-new.component.html',
  styleUrls: ['./service-new.component.scss']
})
export class ServiceNewComponent implements OnInit {

  private latestFormEvent$ = new BehaviorSubject({
    valid: false,
    name: '',
    startDate: ''
  });
  
  isFormValid$ = this.latestFormEvent$.pipe(map((event) => event.valid));

  constructor(
    private servicesFacade: ServicesFacade,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  formChanged(event: FormChangedEvent) {
    this.latestFormEvent$.next(event);
  }

  create() {
    if(!this.latestFormEvent$.value.valid) return; 
    
    // In the real world, the backend will need to create a new service with a new id
    const randomId = Math.random();

    this.servicesFacade.addService({
      id: randomId,
      name: this.latestFormEvent$.value.name,
      startDate: this.latestFormEvent$.value.startDate
    })

    this.router.navigate(['/','services','detail',randomId])
  }
}
