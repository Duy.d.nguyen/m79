import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesComponent } from './services.component';
import { RouterModule, Routes } from '@angular/router';
import { ServicesDataModule } from '@m79/services-data';
import {MatCardModule} from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { ServiceNewComponent } from './service-new/service-new.component';
import { ServiceFormComponent } from './service-form/service-form.component';
import { ServiceEditComponent } from './service-edit/service-edit.component';
import { ServiceDetailComponent } from './service-detail/service-detail.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ServicesComponent
  },
  {
    path: 'edit/:id',
    component: ServiceEditComponent
  },
  {
    path: 'detail/:id',
    component: ServiceDetailComponent
  },
  {
    path: 'new',
    component: ServiceNewComponent
  }
]

@NgModule({
  declarations: [
    ServicesComponent,
    ServiceNewComponent,
    ServiceFormComponent,
    ServiceEditComponent,
    ServiceDetailComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ServicesDataModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatMomentDateModule
  ],
  providers: [
    {provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true }}
  ]
})
export class ServicesModule { }
