import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ServicesEntity, ServicesFacade } from '@m79/services-data';

@Component({
  selector: 'm79-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServicesComponent implements OnInit {

  services$ = this.servicesFacade.allServices$;

  constructor(private servicesFacade: ServicesFacade) {
  }

  ngOnInit(): void {
  }

  trackByServiceContent(index: number, service: ServicesEntity) {
    return '' + service.id + service.name + service.startDate;
  }
}
