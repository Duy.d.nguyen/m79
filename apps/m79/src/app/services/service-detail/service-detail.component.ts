import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesEntity, ServicesFacade } from '@m79/services-data';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'm79-service-detail',
  templateUrl: './service-detail.component.html',
  styleUrls: ['./service-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServiceDetailComponent implements OnInit {

  serviceData$: Observable<ServicesEntity | undefined> = this.activatedRoute.params.pipe(
    switchMap(({id}) => this.servicesFacade.getServiceById(id))
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private servicesFacade: ServicesFacade  
  ) { }

  ngOnInit(): void {
  }

}
