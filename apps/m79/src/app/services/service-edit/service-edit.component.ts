import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesEntity, ServicesFacade } from '@m79/services-data';
import { BehaviorSubject, map, Observable, switchMap } from 'rxjs';
import { first } from 'rxjs/operators';
import { FormChangedEvent } from '../service-form/service-form.model';


@Component({
  selector: 'm79-service-edit',
  templateUrl: './service-edit.component.html',
  styleUrls: ['./service-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,

})
export class ServiceEditComponent implements OnInit {

  serviceData$: Observable<ServicesEntity | undefined> = this.activatedRoute.params.pipe(
    switchMap(({id}) => this.servicesFacade.getServiceById(id))
  );

  private latestFormEvent$ = new BehaviorSubject({
    valid: true,
    name: '',
    startDate: ''
  });

  isFormValid$ = this.latestFormEvent$.pipe(map((event) => event.valid));

  constructor(
    private activatedRoute: ActivatedRoute,
    private servicesFacade: ServicesFacade,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  formChanged(event: FormChangedEvent) {
    this.latestFormEvent$.next(event);
  }

  save(id: number | undefined) {
    if(id !== undefined) {
      // In a real world scenario, a call to the backend would happen here as well.
      this.servicesFacade.updateService({
        id,
        name: this.latestFormEvent$?.value.name,
        startDate: this.latestFormEvent$?.value.startDate
      })

      this.router.navigate(['/','services', 'detail', id])
    }
  }
}

