import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, Credential } from '@m79/auth';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'm79-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {

  hasAuthError$ = new BehaviorSubject<boolean>(false);

  constructor(
    private authService: AuthService,
    private router: Router  
  ) { }

  ngOnInit(): void {
  }

  performAuth(cred: Credential) {
    const success = this.authService.performAuth(cred.username, cred.password);
    this.hasAuthError$.next(!success);

    if(success === true) {
      this.router.navigate(['home']);
    }
  }

}
