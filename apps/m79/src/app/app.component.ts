import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'm79-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'm79';
}
