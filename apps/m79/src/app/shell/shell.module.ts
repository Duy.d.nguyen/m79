import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellComponent } from './shell.component';
import { HeaderComponent } from './header/header.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { RouterModule, Routes } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {MatListModule} from '@angular/material/list';

const routes: Routes = [{
  path: '',
  component: ShellComponent,
  children: [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: 'home'
    },
    {
      path: 'home',
      loadChildren: () => import('@m79/wilson/home/home.module').then(m => m.HomeModule)
    },
    {
      path: 'operations',
      loadChildren: () => import('@m79/wilson/operations/operations.module').then(m => m.OperationsModule)
    },
    {
      path: 'services',
      loadChildren: () => import('@m79/wilson/services/services.module').then(m => m.ServicesModule)
    },
    {
      path: 'shifts',
      loadChildren: () => import('@m79/wilson/shifts/shifts.module').then(m => m.ShiftsModule)
    },
    {
      path: 'user',
      loadChildren: () => import('@m79/wilson/user/user.module').then(m => m.UserModule)
    },
    {
      path: 'vehicles',
      loadChildren: () => import('@m79/wilson/vehicles/vehicles.module').then(m => m.VehiclesModule)
    },
    {
      path: 'locations',
      loadChildren: () => import('@m79/wilson/locations/locations.module').then(m => m.LocationsModule)
    },
    {
      path: 'settings',
      loadChildren: () => import('@m79/wilson/settings/settings.module').then(m => m.SettingsModule)
    }
  ],
  canActivate:  [],
}]

@NgModule({
  declarations: [
    ShellComponent,
    HeaderComponent,
    SideNavComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    RouterModule.forChild(routes)
  ]
})
export class ShellModule { }
