import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'm79-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
